FROM node:11-alpine

WORKDIR /app
COPY . ./

RUN npm run build
RUN npm prune --production

CMD ["npm", "start"]

import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { red, orange } from '@material-ui/core/colors';

import './src/index.css';
import App from './src/components/App';

import store from './redux/store';
require('./redux/bindings');  // Binds Redux actions to the window

window.store = store;

var appDefaultTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#006dc1',
      contrastText: '#FFFFFF'
    },
    secondary: orange,
    error: red,
  },
});

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={appDefaultTheme}>
      <HashRouter>
        <App />
      </HashRouter>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('content')
);

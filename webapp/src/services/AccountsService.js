import axios from 'axios';

class AccountsService {
  constructor(serviceUrl) {
    this.serviceUrl = serviceUrl;
  }

  createAccount(firstName, lastName, emailAddress, username, password) {
    var requestBody = {
      first_name: firstName,
      last_name: lastName,
      email_address: emailAddress,
      username: username,
      password: password
    };

    return new Promise((resolve, reject) => {
      axios({
        url: this.serviceUrl,
        method: 'POST',
        data: requestBody,
        proxy: false
      }).then(_ => resolve())
        .catch(error => {
          if (error.response && error.response.data && error.response.data.error) {
            reject({ error: error.response.data.error });
          } else {
            reject({ error: 'an unexpected error occurred' });
          }
        });
    });
  }

  login(username, password) {
    var requestBody = {
      username: username,
      password: password
    };

    return new Promise((resolve, reject) => {
      axios({
        url: this.serviceUrl + '/login',
        method: 'POST',
        data: requestBody,
        proxy: false
      }).then(response => resolve(response.data.token))
        .catch(error => {
          if (error.response && error.response.data && error.response.data.error) {
            reject({ error: error.response.data.error });
          } else {
            reject({ error: 'an unexpected error occurred' });
          }
        });
    });
  }
}

export default AccountsService;

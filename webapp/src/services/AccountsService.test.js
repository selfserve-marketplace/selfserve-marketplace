import AccountsService from './AccountsService';
import nock from 'nock';

const BASE_PATH = 'http://www.ctsopn.com';

const FIRST_NAME = 'me';
const LAST_NAME = 'myself';
const EMAIL_ADDRESS = 'me@myself.com';
const USERNAME = 'username';
const PASSWORD = 'password';
const VALID_CREATION_BODY = {
  first_name: FIRST_NAME,
  last_name: LAST_NAME,
  email_address: EMAIL_ADDRESS,
  username: USERNAME,
  password: PASSWORD
}

describe('createAccount', () => {
  test('given successful account creation then return promise resolution', async () => {
    nock(BASE_PATH)
      .post('/api/accounts', VALID_CREATION_BODY)
      .reply(201);
    var accountsService = new AccountsService(BASE_PATH + '/api/accounts');

    await accountsService.createAccount(FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, USERNAME, PASSWORD);
  });

  test('given account creation fails then return error', async () => {
    nock(BASE_PATH)
      .post('/api/accounts', VALID_CREATION_BODY)
      .reply(400, { error: 'error' });
    var accountsService = new AccountsService(BASE_PATH + '/api/accounts');

    try {
      await accountsService.createAccount(FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, USERNAME, PASSWORD);
      fail('no error thrown');
    } catch (err) {
      expect(err.error).toBe('error');
    }
  });

  test('given unexpected error then return error', async () => {
    nock(BASE_PATH)
      .post('/api/accounts', VALID_CREATION_BODY)
      .reply(500);
    var accountsService = new AccountsService(BASE_PATH + '/api/accounts');

    try {
      await accountsService.createAccount(FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, USERNAME, PASSWORD);
      fail('no error thrown');
    } catch (err) {
      expect(err.error).toBe('an unexpected error occurred');
    }
  });
});

const VALID_LOGIN_BODY = {
  username: USERNAME,
  password: PASSWORD
}

describe('login', () => {
  test('given successful login then return promise resolution', async () => {
    nock(BASE_PATH)
      .post('/api/accounts/login', VALID_LOGIN_BODY)
      .reply(201);
    var accountsService = new AccountsService(BASE_PATH + '/api/accounts');

    await accountsService.login(USERNAME, PASSWORD)
  });

  test('given wrong credentials then return error', async () => {
    nock(BASE_PATH)
      .post('/api/accounts/login', VALID_LOGIN_BODY)
      .reply(401, { error: 'invalid username or password' });
    var accountsService = new AccountsService(BASE_PATH + '/api/accounts');

    try {
      await accountsService.login(USERNAME, PASSWORD);
      fail('no error thrown');
    } catch (err) {
      expect(err.error).toBe('invalid username or password');
    }
  });

  test('given login fails then return error', async () => {
    nock(BASE_PATH)
      .post('/api/accounts/login', VALID_LOGIN_BODY)
      .reply(400, { error: 'error' });
    var accountsService = new AccountsService(BASE_PATH + '/api/accounts');

    try {
      await accountsService.login(USERNAME, PASSWORD);
      fail('no error thrown');
    } catch (err) {
      expect(err.error).toBe('error');
    }
  });

  test('given unexpected error then return error', async () => {
    nock(BASE_PATH)
      .post('/api/accounts/login', VALID_LOGIN_BODY)
      .reply(500);
    var accountsService = new AccountsService(BASE_PATH + '/api/accounts');

    try {
      await accountsService.login(USERNAME, PASSWORD);
      fail('no error thrown');
    } catch (err) {
      expect(err.error).toBe('an unexpected error occurred');
    }
  });
});

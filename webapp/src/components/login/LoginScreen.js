import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Paper, Typography, Grid, TextField,
  Button
} from '@material-ui/core';

import config from '../../../config';
import AccountsService from '../../services/AccountsService';

const interactiveArea = {
  maxWidth: 500,
  margin: '10px auto 10px',
}

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.accountsService = new AccountsService(config.accountsServiceUrl);

    this._handleFieldContentChanged = this._handleFieldContentChanged.bind(this);
    this._handleFormTypeChanged = this._handleFormTypeChanged.bind(this);
    this._performLogin = this._performLogin.bind(this);
    this._performAccountCreation = this._performAccountCreation.bind(this);

    this.state = {
      isAccountCreation: false,
      username: '',
      password: '',

      firstName: '',
      lastName: '',
      emailAddress: '',
      passwordConfirmation: ''
    }
  }

  _handleFieldContentChanged = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  _handleFormTypeChanged(isAccountCreation) {
    this.setState({
      isAccountCreation: isAccountCreation
    });
  }

  _performLogin() {
    this.accountsService.login(this.state.username, this.state.password)
      .then(token => {
        this.props.updateLoggedInUserToken(token);
        this.props.updateLoggedInUserName(this.state.username);
        this.props.history.push('/home');
      })
      .catch(error => {
        var message = `Error during authentication: ${error.error}`;
        console.log(message);
        this.props.updateErrorMessage(message);
        this.props.setErrorMessageDialogState(true);
      });
  }

  _performAccountCreation() {
    this.accountsService.createAccount(this.state.firstName, this.state.lastName, this.state.emailAddress, this.state.username, this.state.password)
      .catch(error => {
        var message = `Error during account creation: ${error.error}`;
        console.log(message);
        this.props.updateErrorMessage(message);
        this.props.setErrorMessageDialogState(true);
      });
  }

  render() {
    return (
      <div style={interactiveArea}>
        <Paper>
          {!this.state.isAccountCreation &&
            <div style={{ paddingTop: '10' }}>
              <Typography
                variant="h5"
                style={{ textAlign: 'center' }}
              >
                Login
              </Typography>
              {this._renderLoginForm()}
            </div>
          }
          {this.state.isAccountCreation &&
            <div style={{ paddingTop: '10' }}>
              <Typography
                variant="h5"
                style={{ textAlign: 'center' }}
              >
                Account Creation
              </Typography>
              {this._renderAccountCreationForm()}
            </div>
          }
        </Paper>
      </div>
    )
  }

  _renderLoginForm() {
    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="username"
              label="Username"
              value={this.state.username}
              onChange={this._handleFieldContentChanged('username')}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="password"
              label="Password"
              type="password"
              value={this.state.password}
              onChange={this._handleFieldContentChanged('password')}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              disabled={this.state.username === '' || this.state.password === ''}
              onClick={this._performLogin}
            >
              Login
            </Button>
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <Button
              variant="contained"
              color="inherit"
              size="small"
              onClick={() => this._handleFormTypeChanged(true)}
            >
              Create an account
            </Button>
          </Grid>
        </Grid>
      </div>
    )
  }

  _renderAccountCreationForm() {
    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="firstname"
              label="First Name"
              value={this.state.firstName}
              onChange={this._handleFieldContentChanged('firstName')}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="lastname"
              label="Last Name"
              value={this.state.lastName}
              onChange={this._handleFieldContentChanged('lastName')}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="email"
              label="Email Address"
              type="email"
              value={this.state.emailAddress}
              onChange={this._handleFieldContentChanged('emailAddress')}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="username"
              label="Username"
              value={this.state.username}
              onChange={this._handleFieldContentChanged('username')}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="password"
              label="Password"
              type="password"
              value={this.state.password}
              onChange={this._handleFieldContentChanged('password')}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TextField
              id="passwordconfirmation"
              label="Confirm Password"
              type="password"
              value={this.state.passwordConfirmation}
              onChange={this._handleFieldContentChanged('passwordConfirmation')}
              error={this.state.passwordConfirmation !== this.state.password}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              disabled={this.state.firstName === '' || this.state.lastName === ''
                || this.state.emailAddress === '' || this.state.username === ''
                || this.state.password === '' || this.state.passwordConfirmation === ''
                || (this.state.password !== this.state.passwordConfirmation)
              }
              onClick={this._performAccountCreation}
            >
              Submit
            </Button>
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <Button
              variant="contained"
              color="inherit"
              size="small"
              onClick={() => this._handleFormTypeChanged(false)}
            >
              Login with existing account
            </Button>
          </Grid>
        </Grid>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateLoggedInUserToken: token => dispatch(updateLoggedInUserToken(token)),
    updateLoggedInUserName: username => dispatch(updateLoggedInUserName(username)),

    updateErrorMessage: message => dispatch(updateErrorMessage(message)),
    setErrorMessageDialogState: isOpen => dispatch(setErrorMessageDialogState(isOpen))
  };
};

export default connect(null, mapDispatchToProps)(LoginScreen);

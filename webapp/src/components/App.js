import React, { Component } from 'react';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import { Switch, Route, Link } from 'react-router-dom'
import {
  CssBaseline, AppBar, Toolbar, Typography,
  Drawer, List, ListItem, ListItemText,
  Tabs, Tab, Snackbar, IconButton,
  Slide
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core';

import MarketplaceIcon from '../icons/MarketplaceIcon';
import HomePage from './home/HomePage';
import LoginScreen from './login/LoginScreen';

const drawerWidth = 215;

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    paddingLeft: drawerWidth
  },
  toolbar: theme.mixins.toolbar,
});

const PATH_TO_TAB_MAPPINGS = {
  '/home': 0,
};

class App extends Component {
  constructor(props) {
    super(props);

    this._handleTabChange = this._handleTabChange.bind(this);
    this._onErrorMessageDialogClose = this._onErrorMessageDialogClose.bind(this);
    this._performLogout = this._performLogout.bind(this);

    var currentlySelectedTabIndex = this._findCurrentTabFromPath(location.href);
    this.props.updateAppSelectedTab(currentlySelectedTabIndex);
  }

  _findCurrentTabFromPath(currentPath) {
    var navigatedPath = currentPath.split('#')[1];
    var matchingPaths = Object.keys(PATH_TO_TAB_MAPPINGS).filter(path => navigatedPath.startsWith(path));

    if (matchingPaths.length > 1) {
      matchingPaths = matchingPaths.filter(path => path !== '/');
      return PATH_TO_TAB_MAPPINGS[matchingPaths[0]];
    }

    return 0;
  }

  _handleTabChange(event, value) {
    this.props.updateAppSelectedTab(value);
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <AppBar position="sticky" color="primary" className={classes.appBar}>
          <CssBaseline />
          <Toolbar>
            <div style={{ paddingRight: 10 }}>
              <MarketplaceIcon />
            </div>
            <Typography
              variant="h6"
              color="inherit"
              onClick={() => this.props.updateAppSelectedTab(0)}
              style={{ cursor: 'pointer', textDecoration: 'none' }}
              component={Link}
              to="/home"
            >
              OPN Marketplace
            </Typography>

            <Tabs value={this.props.appSelectedTab} onChange={this._handleTabChange}>
              <Tab label="Home" component={Link} to="/home" />
            </Tabs>
          </Toolbar>
        </AppBar>

        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}>
          <div className={classes.toolbar} />
          <List>
            {this.props.loggedInUserToken === undefined &&
              <ListItem
                key="login"
                button
                component={Link}
                to="/login"
              >
                <ListItemText primary="Login" />
              </ListItem>
            }
            {this.props.loggedInUserToken !== undefined &&
              <ListItem
                key="logout"
                button
                onClick={this._performLogout}
              >
                <ListItemText primary="Logout" />
              </ListItem>
            }
          </List>
        </Drawer>

        <main className={classes.content}>

          <Switch>
            <Route path="/home" exact component={HomePage} />
            <Route path="/login" exact component={LoginScreen} />
          </Switch>

        </main>

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          open={this.props.isErrorMessageDialogOpen}
          autoHideDuration={10000}
          onClose={this._onErrorMessageDialogClose}
          ContentProps={{ 'aria-describedby': 'message-id' }}
          message={<span id="message-id">{this.props.currentErrorMessage}</span>}
          TransitionComponent={(props) => <Slide {...props} direction="up" />}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this._onErrorMessageDialogClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
      </div>
    )
  }

  _onErrorMessageDialogClose() {
    this.props.setErrorMessageDialogState(false);
  }

  _performLogout() {
    this.props.updateLoggedInUserToken(undefined);
    this.props.updateLoggedInUserName(undefined);
  }
}

const mapStateToProps = state => {
  return {
    appSelectedTab: state.appSelectedTab,

    loggedInUserToken: state.loggedInUserToken,
    loggedInUserName: state.loggedInUserName,

    currentErrorMessage: state.currentErrorMessage,
    isErrorMessageDialogOpen: state.isErrorMessageDialogOpen
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateAppSelectedTab: index => dispatch(updateAppSelectedTab(index)),

    updateLoggedInUserToken: token => dispatch(updateLoggedInUserToken(token)),
    updateLoggedInUserName: username => dispatch(updateLoggedInUserName(username)),

    setErrorMessageDialogState: isOpen => dispatch(setErrorMessageDialogState(isOpen))
  };
};

const styledAppComponent = withStyles(styles)(App);
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(styledAppComponent));

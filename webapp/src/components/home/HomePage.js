import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Paper, Typography } from '@material-ui/core';

const interactiveArea = {
  maxWidth: 500,
  margin: '10px auto 10px',
}

class HomePage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={interactiveArea}>
        {this.props.loggedInUserName === undefined &&
          this._renderAnonymous()
        }
        {this.props.loggedInUserName !== undefined &&
          this._renderAuthenticated()
        }
      </div>
    )
  }

  _renderAnonymous() {
    return (
      <Paper>
        <Typography
          variant="h5"
          style={{ textAlign: 'center' }}
        >
          Welcome to the OPN marketplace!
      </Typography>
        <br />
        <Typography
          variant="subtitle1"
          style={{ textAlign: 'center' }}
        >
          Please login to access the portal.
      </Typography>
      </Paper>
    )
  }

  _renderAuthenticated() {
    return (
      <Paper>
        <Typography
          variant="h5"
          style={{ textAlign: 'center' }}
        >
          Welcome back {this.props.loggedInUserName}!
        </Typography>
        <br />
        <Typography
          variant="subtitle1"
          style={{ textAlign: 'center' }}
        >
          Lorem ipsum dolor
      </Typography>
      </Paper>
    )
  }
}

const mapStateToProps = state => {
  return {
    loggedInUserName: state.loggedInUserName,
  };
};

export default connect(mapStateToProps, null)(HomePage);

import {
  APP_UPDATE_SELECTED_TAB, LOGGED_IN_USER_TOKEN, LOGGED_IN_USER_NAME,
  ERROR_MESSAGE_UPDATE, ERROR_MESSAGE_DIALOG_STATE
} from '../constants';

const initialState = {
  appSelectedTab: 0,

  loggedInUserToken: undefined,
  loggedInUserName: undefined,

  currentErrorMessage: '',
  isErrorMessageDialogOpen: false
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case APP_UPDATE_SELECTED_TAB:
      return Object.assign({}, state, { appSelectedTab: action.payload });

    case LOGGED_IN_USER_TOKEN:
      return Object.assign({}, state, { loggedInUserToken: action.payload });

    case LOGGED_IN_USER_NAME:
      return Object.assign({}, state, { loggedInUserName: action.payload });

    case ERROR_MESSAGE_UPDATE:
      return Object.assign({}, state, { currentErrorMessage: action.payload });

    case ERROR_MESSAGE_DIALOG_STATE:
      return Object.assign({}, state, { isErrorMessageDialogOpen: action.payload });

    default:
      return state;
  }
}

export default rootReducer;

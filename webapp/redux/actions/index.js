import {
  APP_UPDATE_SELECTED_TAB, LOGGED_IN_USER_TOKEN, LOGGED_IN_USER_NAME,
  ERROR_MESSAGE_UPDATE, ERROR_MESSAGE_DIALOG_STATE
} from '../constants';

export const updateAppSelectedTab = index => ({ type: APP_UPDATE_SELECTED_TAB, payload: index });

export const updateLoggedInUserToken = token => ({ type: LOGGED_IN_USER_TOKEN, payload: token });
export const updateLoggedInUserName = username => ({ type: LOGGED_IN_USER_NAME, payload: username });

export const updateErrorMessage = message => ({ type: ERROR_MESSAGE_UPDATE, payload: message });
export const setErrorMessageDialogState = isOpen => ({ type: ERROR_MESSAGE_DIALOG_STATE, payload: isOpen });

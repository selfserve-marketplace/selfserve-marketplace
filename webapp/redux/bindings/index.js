import {
  updateAppSelectedTab, updateLoggedInUserToken, updateLoggedInUserName,
  updateErrorMessage, setErrorMessageDialogState
} from '../actions';

window.updateAppSelectedTab = updateAppSelectedTab;
window.updateLoggedInUserToken = updateLoggedInUserToken;
window.updateLoggedInUserName = updateLoggedInUserName;
window.updateErrorMessage = updateErrorMessage;
window.setErrorMessageDialogState = setErrorMessageDialogState;

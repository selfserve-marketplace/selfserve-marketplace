const AccountsService = require('./accountsService');
const Account = require('../models/account');
const sha512 = require('js-sha512');

const USERNAME = 'username';
const PASSWORD = 'password';
const FIRST_NAME = 'me';
const LAST_NAME = 'myself';
const EMAIL_ADDRESS = 'me@myself.me';

describe('createAccount', () => {
  test('given valid account information then account is successfully created', async () => {
    Account.create = jest.fn();
    Account.find = jest.fn(() => []);
    var accountsService = new AccountsService();

    var accountId = await accountsService.createAccount(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);

    expect(Account.create).toHaveBeenCalledTimes(1);
    expect(accountId).toEqual(expect.any(String));
  });

  test('given persistence error then return error', async () => {
    Account.create = jest.fn(() => { throw new Error('error') });
    Account.find = jest.fn(() => []);
    var accountsService = new AccountsService();

    try {
      await accountsService.createAccount(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
      fail('No error thrown');
    } catch (err) {
      expect(Account.create).toHaveBeenCalledTimes(1);
      expect(err.message).toEqual(expect.any(String));
    }
  });

  test('given no username then no account is created', async () => {
    Account.create = jest.fn();
    Account.find = jest.fn(() => []);
    var accountsService = new AccountsService();

    try {
      await accountsService.createAccount(undefined, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
      fail('No error thrown');
    } catch (err) {
      expect(err.message).toEqual(expect.any(String));
      expect(Account.create).toHaveBeenCalledTimes(0);
    }
  });

  test('given username already in use then no account is created', async () => {
    Account.create = jest.fn();
    Account.find = jest.fn(() => [{}]);
    var accountsService = new AccountsService();

    try {
      await accountsService.createAccount(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
      fail('No error thrown');
    } catch (err) {
      expect(err.message).toEqual(expect.any(String));
      expect(Account.create).toHaveBeenCalledTimes(0);
    }
  });

  test('given no password then no account is created', async () => {
    Account.create = jest.fn();
    Account.find = jest.fn(() => []);
    var accountsService = new AccountsService();

    try {
      await accountsService.createAccount(USERNAME, undefined, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
      fail('No error thrown');
    } catch (err) {
      expect(err.message).toEqual(expect.any(String));
      expect(Account.create).toHaveBeenCalledTimes(0);
    }
  });

  test('given no firstName then no account is created', async () => {
    Account.create = jest.fn();
    Account.find = jest.fn(() => []);
    var accountsService = new AccountsService();

    try {
      await accountsService.createAccount(USERNAME, PASSWORD, undefined, LAST_NAME, EMAIL_ADDRESS);
      fail('No error thrown');
    } catch (err) {
      expect(err.message).toEqual(expect.any(String));
      expect(Account.create).toHaveBeenCalledTimes(0);
    }
  });

  test('given no lastName then no account is created', async () => {
    Account.create = jest.fn();
    Account.find = jest.fn(() => []);
    var accountsService = new AccountsService();

    try {
      await accountsService.createAccount(USERNAME, PASSWORD, FIRST_NAME, undefined, EMAIL_ADDRESS);
      fail('No error thrown');
    } catch (err) {
      expect(err.message).toEqual(expect.any(String));
      expect(Account.create).toHaveBeenCalledTimes(0);
    }
  });

  test('given no emailAddress then no account is created', async () => {
    Account.create = jest.fn();
    Account.find = jest.fn(() => []);
    var accountsService = new AccountsService();

    try {
      await accountsService.createAccount(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, undefined);
      fail('No error thrown');
    } catch (err) {
      expect(err.message).toEqual(expect.any(String));
      expect(Account.create).toHaveBeenCalledTimes(0);
    }
  });
});

const ACCOUNT_ID = '1';
const ACCOUNT_DOCUMENT = {
  username: USERNAME,
  hashedPassword: PASSWORD,
  firstName: FIRST_NAME,
  lastName: LAST_NAME,
  emailAddress: EMAIL_ADDRESS,
  role: 'USER'
};

describe('findById', () => {
  test('when ID exists then return account information', async () => {
    Account.find = jest.fn(() => [ACCOUNT_DOCUMENT])
    var accountsService = new AccountsService();

    var accountInformation = await accountsService.findById(ACCOUNT_ID);

    var expectedAccountInformation = { username: USERNAME, firstName: FIRST_NAME, lastName: LAST_NAME, emailAddress: EMAIL_ADDRESS };
    expect(accountInformation).toEqual(expectedAccountInformation);
  });

  test('given unknown ID then return undefined', async () => {
    Account.find = jest.fn(() => [])
    var accountsService = new AccountsService();

    var accountInformation = await accountsService.findById(ACCOUNT_ID);

    expect(accountInformation).toBeUndefined();
  });

  test('given persistence error then return error', async () => {
    Account.find = jest.fn(() => { throw new Error('error') })
    var accountsService = new AccountsService();

    try {
      await accountsService.findById(ACCOUNT_ID);
      fail('No error thrown');
    } catch (err) {
      expect(err.message).toEqual(expect.any(String));
    }
  });
});

const ACCOUNT_ROLE = 'USER';
const AUTH_ACCOUNT_DOCUMENT = {
  username: USERNAME,
  hashedPassword: sha512(PASSWORD),
  role: ACCOUNT_ROLE
};

describe('authenticate', () => {
  test('given successful authentication then return account role', async () => {
    Account.find = jest.fn(() => [AUTH_ACCOUNT_DOCUMENT])
    var accountsService = new AccountsService();

    var accountRole = await accountsService.authenticate(USERNAME, PASSWORD);

    expect(accountRole).toEqual(ACCOUNT_ROLE);
  });

  test('given unknown username then return undefined role', async () => {
    Account.find = jest.fn(() => [])
    var accountsService = new AccountsService();

    var accountRole = await accountsService.authenticate(USERNAME, PASSWORD);

    expect(accountRole).toBeUndefined();
  });

  test('given invalid password then return undefined role', async () => {
    Account.find = jest.fn(() => [AUTH_ACCOUNT_DOCUMENT])
    var accountsService = new AccountsService();

    var accountRole = await accountsService.authenticate(USERNAME, 'invalid');

    expect(accountRole).toBeUndefined();
  });
});

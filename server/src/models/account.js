const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AccountSchema = new Schema(
  {
    accountId: {
      type: String,
      index: true
    },
    username: {
      type: String,
      index: true
    },
    hashedPassword: String,
    firstName: String,
    lastName: String,
    emailAddress: String,
    role: {
      type: String,
      default: 'USER'
    }
  },
  { collection: 'accounts' }
);

module.exports = exports = mongoose.model('Account', AccountSchema);

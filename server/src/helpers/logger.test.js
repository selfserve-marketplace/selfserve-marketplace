const winston = require('winston');

describe('Logger bootstrap', () => {
  test('given node environment then bootstrap logger configuration', () => {
    winston.createLogger = jest.fn();
    require('./logger');

    expect(winston.createLogger).toHaveBeenCalledTimes(1);
    expect(winston.createLogger).toHaveBeenLastCalledWith(expect.any(Object));
  });
});

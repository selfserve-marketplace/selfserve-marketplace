const mongoose = require('mongoose');
const config = require('../../config');

describe('Database bootstrap', () => {
  test('given connection URL then bootstrap database connection handlers', () => {
    mongoose.connect = jest.fn();
    mongoose.connection.on = jest.fn();
    mongoose.connection.close = jest.fn();
    require('./db');

    expect(mongoose.connect).toHaveBeenCalledTimes(1);
    expect(mongoose.connect).toHaveBeenLastCalledWith(
      config.mongoDbUrl,
      { useNewUrlParser: true, autoReconnect: true }
    );

    expect(mongoose.connection.on).toHaveBeenCalledTimes(3);
    expect(mongoose.connection.on).toHaveBeenNthCalledWith(1, 'connected', expect.any(Function));
    expect(mongoose.connection.on).toHaveBeenNthCalledWith(2, 'error', expect.any(Function));
    expect(mongoose.connection.on).toHaveBeenNthCalledWith(3, 'disconnected', expect.any(Function));
  });
});

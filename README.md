# selfserve-marketplace [![pipeline status](https://gitlab.com/selfserve-marketplace/selfserve-marketplace/badges/master/pipeline.svg)](https://gitlab.com/selfserve-marketplace/selfserve-marketplace/commits/master) [![coverage report](https://gitlab.com/selfserve-marketplace/selfserve-marketplace/badges/master/coverage.svg)](https://gitlab.com/selfserve-marketplace/selfserve-marketplace/commits/master)

Next generation OPN selfserve sales tool.

## Code Coverage

The latest code coverage report is available [here](https://selfserve-marketplace.gitlab.io/selfserve-marketplace/).
